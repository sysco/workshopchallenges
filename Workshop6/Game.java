// A class for playing the TicTacToe game
public class Game {
	public StringBuffer board;

	public Game(String s) {
		board = new StringBuffer(s);
	}

        // set game with player at position p
	public Game(String s, int p, char player) {
		board = new StringBuffer(s);
		board.setCharAt(p, player);
	}

        // Function for suggesting move for a player
	public int sgstMv(char plr) {
		for (int i = 0; i < 9; i++) {
			if (board.charAt(i) == '-') {
				Game game = makeMv(i, plr);
				if (game.winner() == plr)
					return i;
			}
		}

		for (int i = 0; i < 9; i++) {
			if (board.charAt(i) == '-')
				return i;
		}

		return -1;
	}

	public Game makeMv(int i, char player) {
		return new Game(board.toString(), i, player);
	}

	public char winner() {
		char w = '-'
		boolean wFound = false
		// check for horizontal winner
		for (int i = 0; i < 9; i += 3) {
			if (board.charAt(i) != '-'
					&& board.charAt(i + 1) == board.charAt(i)
					&& board.charAt(i + 2) == board.charAt(i))
			{
				wFound = true;
				w = board.charAt(i);
			}
			if( !wFound && i == 8)
			{
				// check for vertical winner
				for (int i = 0; i < 3; ++i) {
					if (board.charAt(i) != '-'
							&& board.charAt(i + 3) == board.charAt(i)
							&& board.charAt(i + 6) == board.charAt(i))
					{
						wFound = true;
						w = board.charAt(i);
					}
					if( !wFound && i == 2)
					{
						// check for diagonal winner
						if (board.charAt(0) != '-' && board.charAt(4) == board.charAt(0)
								&& board.charAt(8) == board.charAt(0))
						{
							wFound = true;
							w = board.charAt(0);
						}
						if (board.charAt(2) != '-' && board.charAt(4) == board.charAt(2)
								&& board.charAt(6) == board.charAt(2))
						{
							wFound = true;
							w = board.charAt(2);
						}
					}
				}
			}
		}
		
		return w;
	}
}
