import junit.framework.*;

public class GameTest extends TestCase {

	public GameTest(String s) {
		super(s);
	}

	public void testDefaultMove() {
		Game game = new Game("XOXOX-OXO");
		assertEquals(5, game.suggestMove('X'));

		game = new Game("XOXOXOOX-");
		assertEquals(8, game.suggestMove('O'));

		game = new Game("---------");
		assertEquals(0, game.suggestMove('X'));

		game = new Game("XXXXXXXXX");
		assertEquals(-1, game.suggestMove('X'));
	}

	public void testFindWinningMove() {
		Game game = new Game("XO-XX-OO-");
		assertEquals(5, game.suggestMove('X'));
		assertEquals(8, game.suggestMove('O'));
	}

	public void testWinConditions() {
		assertWinner("---XXX---", 'X');
		assertWinner("XXX------", 'X');
		assertWinner("------XXX", 'X');
		assertWinner("X--X--X--", 'X');
		assertWinner("-X--X--X-", 'X');
		assertWinner("--X--X--X", 'X');
		assertWinner("X---X---X", 'X');
		assertWinner("--X-X-X--", 'X');
		
		assertWinner("---OOO---", 'O');
		assertWinner("OOO------", 'O');
		assertWinner("------OOO", 'O');
		assertWinner("O--O--O--", 'O');
		assertWinner("-O--O--O-", 'O');
		assertWinner("--O--O--O", 'O');
		assertWinner("O---O---O", 'O');
		assertWinner("--O-O-O--", 'O');

		assertWinner("---------", '-');
		assertWinner("XXOOOXXOO", '-');
	}
	
	private void assertWinner(String s, char w) {
		Game game = new Game(s);
		assertEquals(w, game.winner());		
	}
}